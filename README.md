Have you ever wanted to 
- parse deeply nested json fields using Jackson?
- easily specify custom parsing without using annotations?

This library will help you!  Here's how it's done:
```kotlin
class MyParser(root: JsonNode) {
  val fname: String by node("root.data.value.0.user.name.first")
  val lname: String by node("root.data.value.0.user.name.last")
}
```

will parse json that looks like
```json
{
  "root": {
    "data": {
      "value": [
        {
          "user": {
            "id": "1",
            "name": {
              "first": "x",
              "last": "y"
            }
          }
        }
      ]
    }
  }
}
```