package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.ResDsl
import tech.codingzen.kata.result.res
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KProperty

typealias ThisRef = Any?
fun interface ContextualParser<SRC : JsonNode?, V> : (SRC, ThisRef, KProperty<*>) -> Res<V>

inline fun <reified V> contextualParser(crossinline block: ResDsl.(JsonNode?, ThisRef, KProperty<*>) -> V) =
  ContextualParser { src: JsonNode?, thisRef, property ->
    res { block(src, thisRef, property) }
  }

fun <V> propertyCheckerInterceptor(parser: ContextualParser<JsonNode, V>) =
  ContextualParser<JsonNode?, V> { src, thisRef, property ->
    res {
      if (src == null || src is NullNode)
        if (property.returnType.isMarkedNullable) null as V
        else err("source was null but property is not marked nullable")
      else
        parser(src, thisRef, property).value()
    }
  }

fun <T> jsonNodeParser(block: ResDsl.(JsonNode) -> T) = ContextualParser<JsonNode, T> { node, _, _ -> res { block(node) } }

object JsonNodeParsers {
  val identity = jsonNodeParser { it }
  val string = jsonNodeParser { node ->
    node.takeIf { it.isTextual }?.textValue() ?: err("JsonNode could not be converted to String")
  }
  val byte = jsonNodeParser { node ->
    node.takeIf { it.isInt }?.intValue()?.toByte() ?: err("cannot parse JsonNode into Byte")
  }
  val uByte = jsonNodeParser { node ->
    node.takeIf { it.isInt }?.intValue()?.toUByte() ?: err("cannot parse JsonNode into UByte")
  }
  val short = jsonNodeParser { node ->
    (node.takeIf { it.isShort } ?: err("cannot parse JsonNode into Short")).shortValue()
  }
  val int = jsonNodeParser { node ->
    (node.takeIf { it.isInt } ?: err("cannot parse JsonNode into Int")).intValue()
  }
  val long = jsonNodeParser { node ->
    (node.takeIf { it.isLong } ?: err("cannot parse JsonNode into Long")).longValue()
  }
  val float = jsonNodeParser { node ->
    (node.takeIf { it.isFloat } ?: err("cannot parse JsonNode into Float")).floatValue()
  }
  val double = jsonNodeParser { node ->
    (node.takeIf { it.isDouble } ?: err("cannot parse JsonNode into Double")).doubleValue()
  }
  val boolean = jsonNodeParser { node ->
    (node.takeIf { it.isBoolean } ?: err("cannot parse JsonNode into Boolean")).booleanValue()
  }
  val uShort = jsonNodeParser { node ->
    (node.takeIf { it.isShort } ?: err("cannot parse JsonNode into UShort")).shortValue().toUShort()
  }
  val uInt = jsonNodeParser { node ->
    (node.takeIf { it.isInt } ?: err("cannot parse JsonNode into UInt")).intValue().toUInt()
  }
  val uLong = jsonNodeParser { node ->
    (node.takeIf { it.isLong } ?: err("cannot parse JsonNode into ULong")).longValue().toULong()
  }
  val char = jsonNodeParser { node ->
    (node.takeIf { it.isTextual } ?: err("cannot parse JsonNode into Char")).textValue()[0]
  }
  val bigInteger = jsonNodeParser<BigInteger> { node ->
    (node.takeIf { it.isBigInteger } ?: err("cannot parse JsonNode into BigInteger")).bigIntegerValue()
  }
  val bigDecimal = jsonNodeParser<BigDecimal> { node ->
    (node.takeIf { it.isBigDecimal } ?: err("cannot parse JsonNode into BigDecimal")).decimalValue()
  }

  inline fun <reified V> parser(): ContextualParser<JsonNode, V> =
    when (val klass = V::class) {
      JsonNode::class -> identity
      String::class -> string
      Byte::class -> byte
      UByte::class -> uByte
      Short::class -> short
      UShort::class -> uShort
      Int::class -> int
      UInt::class -> uInt
      Long::class -> long
      ULong::class -> uLong
      Float::class -> float
      Double::class -> double
      Boolean::class -> boolean
      Char::class -> char
      BigInteger::class -> bigInteger
      BigDecimal::class -> bigDecimal
      else -> jsonNodeParser { err("cannot decode String into type: ${klass.qualifiedName}.  Consider using a custom KonfigDecoder") }
    } as ContextualParser<JsonNode, V>

  inline fun <reified V : Enum<V>, reified T : V?> enum(): ContextualParser<JsonNode, T> = jsonNodeParser { node ->
    val s = node.takeIf { it.isTextual }?.textValue()
    if (s == null) err("cannot parse JsonNode with no text value into enum ${V::class.qualifiedName}")
    else catch { enumValueOf<V>(s) as T } context { "cannot parse JsonNode into enum ${V::class.qualifiedName}" }
  }
}