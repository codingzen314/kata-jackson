package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode
import tech.codingzen.JsonNodeExtensions.invoke
import tech.codingzen.kata.result.*
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadOnlyProperty

class JsonNodeExtensionsException : Exception {
  constructor() : super()
  constructor(message: String) : super(message)
  constructor(message: String, cause: Throwable) : super(message, cause)
  constructor(cause: Throwable) : super(cause)

  companion object {
    operator fun invoke(err: Err): JsonNodeExtensionsException {
      val (messages, cause) = err.parts
      val message = messages.joinToString(System.lineSeparator())
      val exception = if (cause == null) JsonNodeExtensionsException(message) else JsonNodeExtensionsException(message, cause)
      return exception
    }
  }
}

typealias JsonNodeExtPropertyDelegate<V> = ReadOnlyProperty<ThisRef, V>
typealias JsonNodeExtPropertyDelegateProvider<V> = PropertyDelegateProvider<ThisRef, JsonNodeExtPropertyDelegate<V>>

object JsonNodeExtensions {
  @PublishedApi
  internal inline fun <T, V> cached(crossinline block: () -> ReadOnlyProperty<T, V>) =
    PropertyDelegateProvider<T, ReadOnlyProperty<T, V>> { thisRef, property ->
      val supplier = lazy { block().getValue(thisRef, property) }
      ReadOnlyProperty { _, _ -> supplier.value }
    }

  inline operator fun <reified V> JsonNode.invoke(nav: JsonNav, parser: ContextualParser<JsonNode?, V>): JsonNodeExtPropertyDelegateProvider<V> =
    cached {
      ReadOnlyProperty { thisRef, property ->
        when (val outcome = parser(nav(this), thisRef, property)) {
          is Ok -> outcome.value
          is Err -> {
            val className = thisRef?.let { it::class.qualifiedName } ?: "anonymous-type"
            throw JsonNodeExtensionsException(outcome.context("Attempting to set value for $className.${property.name}"))
          }
        }
      }
    }


  @JvmName("invoke_jsonnav")
  inline operator fun <reified V> JsonNode.invoke(nav: JsonNav, parser: ContextualParser<JsonNode, V>): JsonNodeExtPropertyDelegateProvider<V> =
    this(nav, propertyCheckerInterceptor(parser))

  inline operator fun <reified V> JsonNode.invoke(nav: JsonNav): PropertyDelegateProvider<Any?, ReadOnlyProperty<Any?, V>> =
    this(nav, JsonNodeParsers.parser())

  @JvmName("invoke")
  inline operator fun <reified V> JsonNode.invoke(nav: JsonNav, crossinline konstructor: (JsonNode) -> V): JsonNodeExtPropertyDelegateProvider<V> =
    this(nav) { node: JsonNode, _, _ ->
      res {
        catch { konstructor(node) } context { "Error calling konstructor for type ${V::class.java}" }
      }
    }


  inline operator fun <reified V> JsonNode.invoke(path: String, parser: ContextualParser<JsonNode?, V>): JsonNodeExtPropertyDelegateProvider<V> =
    this(Path(path), parser)

  @JvmName("invoke_string")
  inline operator fun <reified V> JsonNode.invoke(path: String, parser: ContextualParser<JsonNode, V>): JsonNodeExtPropertyDelegateProvider<V> =
    this(Path(path), parser)

  inline operator fun <reified V> JsonNode.invoke(path: String): PropertyDelegateProvider<Any?, ReadOnlyProperty<Any?, V>> =
    this(Path(path))

  inline operator fun <reified V> JsonNode.invoke(path: String, crossinline konstructor: (JsonNode) -> V): JsonNodeExtPropertyDelegateProvider<V> =
    this(Path(path), konstructor)


  inline operator fun <reified V> JsonNode.invoke(parser: ContextualParser<JsonNode?, V>): JsonNodeExtPropertyDelegateProvider<V> =
    PropertyDelegateProvider { thisRef, property -> this(property.name, parser).provideDelegate(thisRef, property) }

  @JvmName("invoke_property")
  inline operator fun <reified V> JsonNode.invoke(parser: ContextualParser<JsonNode, V>): JsonNodeExtPropertyDelegateProvider<V> =
    PropertyDelegateProvider { thisRef, property -> this(property.name, parser).provideDelegate(thisRef, property) }

  inline operator fun <reified V> JsonNode.invoke(): PropertyDelegateProvider<Any?, ReadOnlyProperty<Any?, V>> =
    PropertyDelegateProvider { thisRef, property -> this@invoke<V>(property.name).provideDelegate(thisRef, property) }

  inline operator fun <reified V> JsonNode.invoke(crossinline konstructor: (JsonNode) -> V): JsonNodeExtPropertyDelegateProvider<V> =
    PropertyDelegateProvider { thisRef, property -> this(property.name, konstructor).provideDelegate(thisRef, property) }
}