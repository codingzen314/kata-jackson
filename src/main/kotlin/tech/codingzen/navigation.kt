package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode

sealed interface JsonNav {
  companion object {
    fun key(key: String) = Key(key)
    fun idx(idx: Int) = Idx(idx)
  }

  operator fun invoke(node: JsonNode): JsonNode? =
    when (this@JsonNav) {
      is Key -> node[key]
      is Idx -> node[idx]
      is Path -> {
        var current: JsonNode? = node
        for (dir in path) {
          current = current?.let { (dir as JsonNav).invoke(it) }
        }
        current
      }
    }

  fun pprint(): String = when (this) {
    is Key -> "$key"
    is Idx -> "$idx"
    is Path -> path.joinToString(".") { (it as JsonNav).pprint() }
  }
}

sealed interface JsonDir

@JvmInline
value class Key internal constructor(val key: String) : JsonDir, JsonNav {
  override fun pprint() = key
}

@JvmInline
value class Idx internal constructor(val idx: Int) : JsonDir, JsonNav {
  override fun pprint() = idx.toString()
}

@JvmInline
value class Path(val path: List<JsonDir>) : JsonNav {
  companion object {
    operator fun invoke(path: String): JsonNav =
      path
        .split('.')
        .map { s -> s.toIntOrNull()?.let { Idx(it) } ?: Key(s) }
        .let(::Path)
  }
}