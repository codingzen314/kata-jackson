//package tech.codingzen
//
//import com.fasterxml.jackson.databind.JsonNode
//import com.fasterxml.jackson.databind.ObjectMapper
//import tech.codingzen.JsonNodeExtensions.invoke
//import tech.codingzen.JsonNodeParsers.enum
//import kotlin.reflect.full.declaredMemberProperties
//
//class CustomParser(node: JsonNode) {
//  val x: Int by node()
//  val y: String by node("str")
//  val strNull: String? by node("str-null")
//  val color: Color by node("clr", enum())
//  val colorOrNull: Color? by node("clr2", enum())
//
//}
//
//enum class Color {
//  RED, GREEN, BLUE
//}
//
//fun main(args: Array<String>) {
//  println("Hello, World")
//  val parsed = CustomParser(ObjectMapper().readTree(json))
//  println(CustomParser::class.declaredMemberProperties.map { it.name to it.get(parsed) }.joinToString(prefix = "CustomParser(", postfix = ")") { (name, value) -> "$name: $value" })
//}
//
//val json = """
//  {
//    "x": 1,
//    "str": "hi",
//    "str-ull": null,
//    "clr": "RED"
//  }
//""".trimIndent()
//
