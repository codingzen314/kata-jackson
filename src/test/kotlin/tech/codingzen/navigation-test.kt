package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class NavigationTest {
  @Test
  fun `key invoke`() {
    test("""{}""") {
      assertNull(JsonNav.key("a")(it))
    }
    test("""{"a": 1}""") {
      assertNotNull(JsonNav.key("a")(it))
    }
  }

  @Test
  fun `idx invoke`() {
    test("""[]""") {
      assertNull(JsonNav.idx(0)(it))
    }
    test("""[1]""") {
      assertNotNull(JsonNav.idx(0)(it))
    }
  }

  @Test
  fun `path invoke`() {
    test("""[1]""") {
      assertNull(Path("a")(it))
    }
    test("""{"a": [{"b": "abc"}]}""") {
      assertEquals("abc", Path("a.0.b")(it)?.textValue())
    }
  }

  inline fun test(json: String, block: (JsonNode) -> Unit): Unit {
    val root = ObjectMapper().readTree(json)
    block(root)
  }
}