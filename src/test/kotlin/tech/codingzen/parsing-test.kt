package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.*
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import tech.codingzen.JsonNodeParsers.bigDecimal
import tech.codingzen.JsonNodeParsers.bigInteger
import tech.codingzen.JsonNodeParsers.boolean
import tech.codingzen.JsonNodeParsers.byte
import tech.codingzen.JsonNodeParsers.char
import tech.codingzen.JsonNodeParsers.double
import tech.codingzen.JsonNodeParsers.enum
import tech.codingzen.JsonNodeParsers.float
import tech.codingzen.JsonNodeParsers.int
import tech.codingzen.JsonNodeParsers.long
import tech.codingzen.JsonNodeParsers.parser
import tech.codingzen.JsonNodeParsers.short
import tech.codingzen.JsonNodeParsers.string
import tech.codingzen.JsonNodeParsers.uByte
import tech.codingzen.JsonNodeParsers.uInt
import tech.codingzen.JsonNodeParsers.uLong
import tech.codingzen.JsonNodeParsers.uShort
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KProperty
import kotlin.reflect.KType

class JsonNodeParsersTest {
  fun String.toTextNode(): JsonNode = TextNode(this)
  fun Int.toByteNode(): JsonNode = IntNode(this)
  fun Int.toUByteNode(): JsonNode = IntNode(this)
  fun Int.toIntNode(): JsonNode = IntNode(this)
  fun Int.toUIntNode(): JsonNode = IntNode(this)
  fun Short.toShortNode(): JsonNode = ShortNode(this)
  fun Short.toUShortNode(): JsonNode = ShortNode(this)
  fun Long.toLongNode(): JsonNode = LongNode(this)
  fun Long.toULongNode(): JsonNode = LongNode(this)
  fun Float.toFloatNode(): JsonNode = FloatNode(this)
  fun Double.toDoubleNode(): JsonNode = DoubleNode(this)
  fun Boolean.toBooleanNode(): JsonNode = if (this) BooleanNode.TRUE else BooleanNode.FALSE
  fun BigInteger.toBigIntegerNode(): JsonNode = BigIntegerNode(this)
  fun BigDecimal.toBigDecimalNode(): JsonNode = DecimalNode(this)

  @Test
  fun stringOrNull() {
    assertOk("abc", string("abc".toTextNode(), null, mock()))
  }

  @Test
  fun byteOrNullTest() {
    assertOk(1.toByte(), byte(1.toByteNode(), null, mock()))
    assertIsErr(byte("a".toTextNode(), null, mock()))
  }

  @Test
  fun uByteOrNullTest() {
    assertOk(2.toUByte(), uByte(2.toUByteNode(), null, mock()))
    assertIsErr(uByte("a".toTextNode(), null, mock()))
  }

  @Test
  fun shortOrNullTest() {
    assertOk(1.toShort(), short(1.toShort().toShortNode(), null, mock()))
    assertIsErr(short("a".toTextNode(), null, mock()))
  }

  @Test
  fun uShortOrNullTest() {
    assertOk(1.toUShort(), uShort(1.toShort().toUShortNode(), null, mock()))
    assertIsErr(uShort("a".toTextNode(), null, mock()))
  }

  @Test
  fun intOrNullTest() {
    assertOk(1, int(1.toIntNode(), null, mock()))
    assertIsErr(int("a".toTextNode(), null, mock()))
  }

  @Test
  fun uIntOrNullTest() {
    assertOk(2.toUInt(), uInt(2.toUIntNode(), null, mock()))
    assertIsErr(uInt("a".toTextNode(), null, mock()))
  }

  @Test
  fun longOrNullTest() {
    assertOk(1.toLong(), long(1L.toLongNode(), null, mock()))
    assertIsErr(long("a".toTextNode(), null, mock()))
  }

  @Test
  fun uLongOrNullTest() {
    assertOk(2.toULong(), uLong(2L.toULongNode(), null, mock()))
    assertIsErr(uLong("a".toTextNode(), null, mock()))
  }

  @Test
  fun floatOrNullTest() {
    assertIsOk(float(1.354f.toFloatNode(), null, mock()))
    assertIsErr(float("a".toTextNode(), null, mock()))
  }

  @Test
  fun doubleOrNullTest() {
    assertIsOk(double(2.345.toDoubleNode(), null, mock()))
    assertIsErr(double("a".toTextNode(), null, mock()))
  }

  @Test
  fun booleanOrNullTest() {
    assertOk(true, boolean(true.toBooleanNode(), null, mock()))
    assertOk(false, boolean(false.toBooleanNode(), null, mock()))
    assertIsErr(boolean("1".toTextNode(), null, mock()))
  }

  @Test
  fun charOrNullTest() {
    assertOk('c', char("cat".toTextNode(), null, mock()))
    assertIsErr(char("".toTextNode(), null, mock()))
  }

  @Test
  fun bigIntegerOrNullTest() {
    assertOk(1.toBigInteger(), bigInteger(1.toBigInteger().toBigIntegerNode(), null, mock()))
    assertIsErr(bigInteger("a".toTextNode(), null, mock()))
  }

  @Test
  fun bigDecimalOrNullTest() {
    assertOk(BigDecimal("1.23"), bigDecimal("1.23".toBigDecimal().toBigDecimalNode(), null, mock()))
    assertIsErr(bigDecimal("a".toTextNode(), null, mock()))
  }

  @Test
  fun parserTest() {
    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn true
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }

      assertOk("abc", parser<String>()("abc".toTextNode(), null, mock()))
      assertOk(1.toByte(), parser<Byte>()(1.toByteNode(), null, mock()))
      assertIsErr(parser<Byte>()("a".toTextNode(), null, mock()))
      assertOk(2.toUByte(), parser<UByte>()(2.toUByteNode(), null, mock()))
      assertIsErr(parser<UByte>()("a".toTextNode(), null, mock()))
      assertOk(1.toShort(), parser<Short>()(1.toShort().toShortNode(), null, mock()))
      assertIsErr(parser<Short>()("a".toTextNode(), null, mock()))
      assertOk(1.toUShort(), parser<UShort>()(1.toShort().toUShortNode(), null, mock()))
      assertIsErr(parser<UShort>()("a".toTextNode(), null, mock()))
      assertOk(1, parser<Int>()(1.toIntNode(), null, mock()))
      assertIsErr(parser<Int>()("a".toTextNode(), null, mock()))
      assertOk(2.toUInt(), parser<UInt>()(2.toUIntNode(), null, mock()))
      assertIsErr(parser<UInt>()("a".toTextNode(), null, mock()))
      assertOk(1.toLong(), parser<Long>()(1L.toLongNode(), null, mock()))
      assertIsErr(parser<Long>()("a".toTextNode(), null, mock()))
      assertOk(2.toULong(), parser<ULong>()(2L.toULongNode(), null, mock()))
      assertIsErr(parser<ULong>()("a".toTextNode(), null, mock()))
      assertIsOk(parser<Float>()(1.354f.toFloatNode(), null, mock()))
      assertIsErr(parser<Float>()("a".toTextNode(), null, mock()))
      assertIsOk(parser<Double>()(2.345.toDoubleNode(), null, mock()))
      assertIsErr(parser<Double>()("a".toTextNode(), null, mock()))
      assertOk(true, parser<Boolean>()(true.toBooleanNode(), null, mock()))
      assertOk(false, parser<Boolean>()(false.toBooleanNode(), null, mock()))
      assertIsErr(parser<Boolean>()("1".toTextNode(), null, mock()))
      assertOk('c', parser<Char>()("cat".toTextNode(), null, mock()))
      assertIsErr(parser<Char>()("".toTextNode(), null, mock()))
      assertOk(1.toBigInteger(), parser<BigInteger>()(1.toBigInteger().toBigIntegerNode(), null, mock()))
      assertIsErr(parser<BigInteger>()("a".toTextNode(), null, mock()))
      assertOk(BigDecimal("1.23"), parser<BigDecimal>()("1.23".toBigDecimal().toBigDecimalNode(), null, mock()))
      assertIsErr(parser<BigDecimal>()("a".toTextNode(), null, mock()))
    }

    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn false
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }

      assertOk("abc", parser<String>()("abc".toTextNode(), null, mock()))
      assertOk(1.toByte(), parser<Byte>()(1.toByteNode(), null, mock()))
      assertIsErr(parser<Byte>()("a".toTextNode(), null, mock()))
      assertOk(2.toUByte(), parser<UByte>()(2.toUByteNode(), null, mock()))
      assertIsErr(parser<UByte>()("a".toTextNode(), null, mock()))
      assertOk(1.toShort(), parser<Short>()(1.toShort().toShortNode(), null, mock()))
      assertIsErr(parser<Short>()("a".toTextNode(), null, mock()))
      assertOk(1.toUShort(), parser<UShort>()(1.toShort().toUShortNode(), null, mock()))
      assertIsErr(parser<UShort>()("a".toTextNode(), null, mock()))
      assertOk(1, parser<Int>()(1.toIntNode(), null, mock()))
      assertIsErr(parser<Int>()("a".toTextNode(), null, mock()))
      assertOk(2.toUInt(), parser<UInt>()(2.toUIntNode(), null, mock()))
      assertIsErr(parser<UInt>()("a".toTextNode(), null, mock()))
      assertOk(1.toLong(), parser<Long>()(1L.toLongNode(), null, mock()))
      assertIsErr(parser<Long>()("a".toTextNode(), null, mock()))
      assertOk(2.toULong(), parser<ULong>()(2L.toULongNode(), null, mock()))
      assertIsErr(parser<ULong>()("a".toTextNode(), null, mock()))
      assertIsOk(parser<Float>()(1.354f.toFloatNode(), null, mock()))
      assertIsErr(parser<Float>()("a".toTextNode(), null, mock()))
      assertIsOk(parser<Double>()(2.345.toDoubleNode(), null, mock()))
      assertIsErr(parser<Double>()("a".toTextNode(), null, mock()))
      assertOk(true, parser<Boolean>()(true.toBooleanNode(), null, mock()))
      assertOk(false, parser<Boolean>()(false.toBooleanNode(), null, mock()))
      assertIsErr(parser<Boolean>()("1".toTextNode(), null, mock()))
      assertOk('c', parser<Char>()("cat".toTextNode(), null, mock()))
      assertIsErr(parser<Char>()("".toTextNode(), null, mock()))
      assertOk(1.toBigInteger(), parser<BigInteger>()(1.toBigInteger().toBigIntegerNode(), null, mock()))
      assertIsErr(parser<BigInteger>()("a".toTextNode(), null, mock()))
      assertOk(BigDecimal("1.23"), parser<BigDecimal>()("1.23".toBigDecimal().toBigDecimalNode(), null, mock()))
      assertIsErr(parser<BigDecimal>()("a".toTextNode(), null, mock()))
    }
  }

  @Test
  fun enumForTest() {
    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn true
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }
      println(property.returnType.isMarkedNullable)
      assertOk(TestEnumA1.Red, enum<TestEnumA1, TestEnumA1>()("Red".toTextNode(), null, property))
      assertIsErr(enum<TestEnumA1, TestEnumA1>()("1".toTextNode(), null, property))
    }

    run {
      val nonNullType = mock<KType> {
        on { isMarkedNullable } doReturn false
      }
      val property = mock<KProperty<*>> {
        on { returnType } doReturn nonNullType
      }
      println(property.returnType.isMarkedNullable)
      assertOk(TestEnumA1.Red, enum<TestEnumA1, TestEnumA1>()("Red".toTextNode(), null, property))
      assertIsErr(enum<TestEnumA1, TestEnumA1>()("1".toTextNode(), null, property))
    }
  }
}

enum class TestEnumA1 {
  Red, Green, Blue
}