package tech.codingzen

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import tech.codingzen.JsonNodeExtensions.invoke
import tech.codingzen.kata.result.Res
import tech.codingzen.kata.result.ok
import kotlin.reflect.KProperty
import kotlin.reflect.KType
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class Foo(node: JsonNode) {
  companion object {
    val parser = contextualParser { node: JsonNode?, _, _ -> null as String? }
  }

  val a: String by node(Path("a"))
}

class HelloTest {
  val thisRef: ThisRef = null

  @Test
  fun `nav err parser`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val message = "my error"
      val parser = contextualParser<String?> { _, _, _ -> err(message) }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke(Path("a"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }
  }

  @Test
  fun `nav ctxP nullable node`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> null as String? }
      assertNull(root.invoke(Path("b"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> node?.textValue() }
      val actual = root.invoke(Path("a"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      assertEquals("abc", actual)
    }
  }

  @Test
  fun `nav ctxP nonnull node`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke(Path("b"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke(Path("b"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke(Path("a"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String> { it.textValue() }
      assertEquals("abc", root.invoke(Path("a"), parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `nav standard parser`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      assertNull(root.invoke<String>(Path("b")).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      assertNull(root.invoke<Int?>(Path("b")).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>(Path("a")).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      assertEquals("abc", root.invoke<String>(Path("a")).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `nav ctxP konstructor`() {
    test(NULLABLE, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertNull(root.invoke<Foo>(Path("b"), ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      assertNull(root.invoke(Path("a"), ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>(Path("a")).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertEquals("abc", root.invoke(Path("c"), ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop).a)
    }
  }

  //TODO: string path

  @Test
  fun `path err parser`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val message = "my error"
      val parser = contextualParser<String?> { _, _, _ -> err(message) }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke("a", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }
  }

  @Test
  fun `path ctxP nullable node`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> null as String? }
      assertNull(root.invoke("b", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> node?.textValue() }
      val actual = root.invoke("a", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      assertEquals("abc", actual)
    }
  }

  @Test
  fun `path ctxP nonnull node`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke("b", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke("b", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke("a", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String> { it.textValue() }
      assertEquals("abc", root.invoke("a", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `path standard parser`() {
    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      assertNull(root.invoke<String>("b").provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      assertNull(root.invoke<Int?>("b").provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>("a").provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"a": "abc"}""") { prop, root ->
      assertEquals("abc", root.invoke<String>("a").provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `path ctxP konstructor`() {
    test(NULLABLE, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertNull(root.invoke<Foo>("b", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      assertNull(root.invoke("a", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>("a").provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertEquals("abc", root.invoke("c", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop).a)
    }
  }

  //TODO: no path
  @Test
  fun `no path err parser`() {
    test(NULLABLE, "a", """{"a": "abc"}""") { prop, root ->
      val message = "my error"
      val parser = contextualParser<String?> { _, _, _ -> err(message) }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }
  }

  @Test
  fun `no path ctxP nullable node`() {
    test(NULLABLE, "b", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> null as String? }
      assertNull(root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": "abc"}""") { prop, root ->
      val parser = contextualParser { node: JsonNode?, _, _ -> node?.textValue() }
      val actual = root.invoke("a", parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      assertEquals("abc", actual)
    }
  }

  @Test
  fun `no path ctxP nonnull node`() {
    test(NULLABLE, "b", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "b", """{"a": null}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertNull(root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "a", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String?> { err("not thrown") }
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "a", """{"a": "abc"}""") { prop, root ->
      val parser = jsonNodeParser<String> { it.textValue() }
      assertEquals("abc", root.invoke(parser).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `no path standard parser`() {
    test(NULLABLE, "b", """{"a": "abc"}""") { prop, root ->
      assertNull(root.invoke<String>().provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "b", """{"a": null}""") { prop, root ->
      assertNull(root.invoke<Int?>().provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "a", """{"a": "abc"}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>().provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "a", """{"a": "abc"}""") { prop, root ->
      assertEquals("abc", root.invoke<String>().provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }
  }

  @Test
  fun `no path ctxP konstructor`() {
    test(NULLABLE, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertNull(root.invoke<Foo>("b", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NULLABLE, "fname", """{"a": null}""") { prop, root ->
      assertNull(root.invoke("a", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop))
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertFailsWith<JsonNodeExtensionsException> {
        root.invoke<Foo>("a").provideDelegate(thisRef, prop).getValue(thisRef, prop)
      }
    }

    test(NONNULL, "fname", """{"c": {"a": "abc"}}""") { prop, root ->
      assertEquals("abc", root.invoke("c", ::Foo).provideDelegate(thisRef, prop).getValue(thisRef, prop).a)
    }
  }

}

val NULLABLE = true
val NONNULL = false

fun test(isNullable: Boolean, propertyName: String, json: String, block: (KProperty<*>, JsonNode) -> Unit) =
  TestSetup(isNullable, propertyName, json)(block)

data class TestSetup(val isNullable: Boolean, val propertyName: String, val json: String) {
  operator fun invoke(block: (KProperty<*>, JsonNode) -> Unit) {
    val nonNullType = mock<KType> {
      on { isMarkedNullable } doReturn isNullable
    }
    val property = mock<KProperty<*>> {
      on { returnType } doReturn nonNullType
      on { name } doReturn propertyName
    }

    block(property, ObjectMapper().readTree(json))
  }
}